package com.igs.auto.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.igs.auto.domain.Bet;


@Repository
public interface BetRepository extends JpaRepository< Bet, Long > {

    Optional< Bet > findOneByNickname( String nickname );


    Optional< Bet > findOneByBets( String bets );
}
