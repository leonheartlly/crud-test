package com.igs.auto.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.igs.auto.domain.Currency;


@Repository
public interface CurrencyRepository extends JpaRepository< Currency, String > {
}
