package com.igs.auto.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.igs.auto.domain.Game;


@Repository
public interface GameRepository extends JpaRepository< Game, Long > {

    Optional< Game > findOneByGameId( String gameid );

	/**
	 * Find all games by Id list.
	 * @param gameIds Id list.
	 * @param type Cast class type.
	 * @param <T> Return type.
	 * @return Results.
	 */
    < T > List< T > findAllByGameIdIn( List<String> gameIds, Class< T > type );
}
