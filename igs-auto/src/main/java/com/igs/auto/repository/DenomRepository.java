package com.igs.auto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.igs.auto.domain.Denom;


@Repository
public interface DenomRepository extends JpaRepository< Denom, Long > {

}
