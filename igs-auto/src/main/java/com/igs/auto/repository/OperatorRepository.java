package com.igs.auto.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.igs.auto.domain.Operator;

@Repository
public interface OperatorRepository extends JpaRepository< Operator, Long > {

	Optional<Operator> findByOperatorId(String operatorId);
}
