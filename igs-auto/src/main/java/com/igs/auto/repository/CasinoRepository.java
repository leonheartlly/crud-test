package com.igs.auto.repository;


import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.igs.auto.domain.Casino;
import com.igs.auto.dto.projection.CasinoDetail;


@Repository
public interface CasinoRepository extends JpaRepository< Casino, Long > {

    List< Casino > findByOperatorOperatorId( String operatorId );


    CasinoDetail findByName( String name );


    < T > List< T > findByIsCasinoEnabled( boolean isEnabled, Class< T > type );


    < T > T findByNameAndOperatorOperatorId( String name, String operatorId, Class< T > type );


    < T > List< T > findAllProjectedBy( Class< T > projection );
}
