package com.igs.auto.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.igs.auto.domain.ProductContact;

import java.util.List;


@Repository
public interface ProductContactRepository extends JpaRepository< ProductContact, Long > {

    /**
     * Look for a contact list by its operator id.
     * 
     * @param operatorId
     *            operator id.
     * @param type
     *            object in which the result will be casted.
     * @param <T>
     *            generic object
     * @return contact list.
     */
    < T > List< T > findByOperatorOperatorId( String operatorId, Class< T > type );


    /**
     * Look for a contact list by its casino name.
     * 
     * @param casinoName
     *            casino's name.
     * @param type
     *            object in which the result will be casted.
     * @param <T>
     *            generic object
     * @return contact list.
     */
    < T > List< T > findByCasinoName( String casinoName, Class< T > type );

    < T > List< T > findByOperatorOperatorIdAndArea( String operatorId, String area, Class< T > type );
}
