package com.igs.auto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.igs.auto.domain.MathConfig;


@Repository
public interface MathConfigRepository extends JpaRepository< MathConfig, Long > {
}
