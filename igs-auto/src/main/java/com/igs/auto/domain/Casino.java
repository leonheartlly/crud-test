package com.igs.auto.domain;


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;


/**
 * Entity Casino. Represents the entire casino information data.
 */
@Entity
@Data
public class Casino implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( length = 35, nullable = false )
    private String name;

    @Column( length = 3 )
    private String code;

    @Column( length = 4, nullable = false )
    private String casinoId;

    @Column( length = 5 )
    private String softlock;

    private boolean reportEnabled;

    private boolean recoverSessionEnabled;

    @Column( name = "desktop_template", length = 120, nullable = false )
    private String desktopTemplate;

    @Column( name = "mobile_template", length = 120, nullable = false )
    private String mobileTemplate;

    private boolean isCasinoEnabled;

    private Timestamp casinoLastEnableDate;

    private Timestamp casinoCreationDate;

    @Column( length = 120 )
    private String casinoURL;

    @Column( length = 50 )
    private String casinoLogin;

    @Column( length = 25 )
    private String casinoPwd;

    @Column( length = 100 )
    private String apiStagingURL;

    @Column( length = 100 )
    private String apiProdURL;

    @JsonIgnore
    @JoinTable( name = "CASINO_CURRENCY", joinColumns = @JoinColumn( name = "casino_id" ), inverseJoinColumns = @JoinColumn( name = "currency_id" ) )
    @ManyToMany
    private List< Currency > currencies = new ArrayList<>();

    @JsonIgnore
    @JoinTable( name = "CASINO_GAME", joinColumns = @JoinColumn( name = "casino_id" ), inverseJoinColumns = @JoinColumn( name = "game_id" ) )
    @ManyToMany
    private List< Game > games = new ArrayList<>();

    @JsonIgnore
    @OneToMany( mappedBy = "casino" )
    private List< ProductContact > productContacts;

    @ManyToOne
    @JoinColumn( name = "operator_id", nullable = false, referencedColumnName = "operatorId" )
    private Operator operator;

}
