package com.igs.auto.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;


@Entity
@Data
public class ProductContact {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( length = 50, nullable = false )
    private String contactName;

    @Column( length = 25, nullable = false )
    private String area;

    @Column( length = 50 )
    private String email;

    @Column( length = 50 )
    private String skypeGroup;

    @ManyToOne
    @JoinColumn( name = "casino_name")
    private Casino casino;

    @ManyToOne
    @JoinColumn( name = "operator_id", referencedColumnName = "operatorId")
    private Operator operator;

}
