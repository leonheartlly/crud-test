package com.igs.auto.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.igs.auto.domain.enums.GameType;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Game {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( length = 35, nullable = false, unique = true )
    private String game;

    @Column( length = 3, nullable = false, unique = true )
    private String gameId;

    private Integer gameType;

    private int ortizNetModelNumber;

    @ManyToOne
    @JoinColumn( name = "id_mathConfig" )
    private MathConfig mathConfig;

    @JsonIgnore
    @ManyToMany( mappedBy = "games" )
    private List< Casino > casinos = new ArrayList<>();



    public void setGameType( GameType gameType ) {

        this.gameType = gameType.getCode();
    }


    public GameType getGameType() {

        return GameType.getGameType( gameType );
    }

}
