package com.igs.auto.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;


@Entity
@Data
public class Bet {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( length = 50, nullable = false, unique = true )
    private String bets;

    @Column( length = 25, nullable = false, unique = true )
    private String nickname;

    @Column( length = 50 )
    private String description;

    @JsonIgnore
    @OneToMany( mappedBy = "bet" )
    private List< MathConfig > mathConfig;


    @Override
    public String toString() {

        return bets;
    }
}
