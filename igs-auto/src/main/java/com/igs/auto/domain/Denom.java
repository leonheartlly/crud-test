package com.igs.auto.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;


@Entity
@Data
public class Denom {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( length = 50, nullable = false )
    private String denoms;

    @JsonIgnore
    @JoinTable( name = "DENOM_CURRENCY", joinColumns = @JoinColumn( name = "denom_id" ), inverseJoinColumns = @JoinColumn( name = "currency_id" ) )
    @ManyToMany
    private List< Currency > currencies = new ArrayList<>();

    @JsonIgnore
    @OneToMany( mappedBy = "denom" )
    private List< MathConfig > mathConfig;
}
