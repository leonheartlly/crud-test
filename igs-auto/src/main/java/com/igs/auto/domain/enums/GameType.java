package com.igs.auto.domain.enums;


import java.util.Optional;


public enum GameType {

    BINGO( 1, "BINGO" ),
    SLOT( 2, "SLOT" ),
    LOTTO(3, "LOTTO");

    private int code;

    private String desc;


    GameType( int code, String desc ) {

        this.code = code;
        this.desc = desc;
    }


    public int getCode() {

        return code;
    }


    public String getDesc() {

        return desc;
    }


    public static GameType getGameType( Integer cod ) {

        if ( !Optional.ofNullable( cod ).isPresent() ) {
            return null;
        }

        for ( GameType gt : GameType.values() ) {
            if ( cod.equals( gt.getCode() ) ) {
                return gt;
            }
        }
        throw new IllegalArgumentException( "Id inválido: " + cod );
    }

}
