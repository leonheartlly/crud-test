package com.igs.auto.domain;


import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;


@Entity
@Data
public class Operator implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( length = 35, nullable = false )
    private String name;

    @Column( length = 15, nullable = false )
    private String operatorId;

    private boolean isTestingOperator;

    private boolean isIsTestingOperator;

    private boolean saveHistory;

    private boolean retryEnabled;

    private boolean instantDeposit;

    private boolean sendZeroDeposit; @JsonIgnore

    @Column( length = 120, nullable = false )
    private String gamePath;

    @Column( length = 120, nullable = false )
    private String endpoint;

    @JsonIgnore
    @OneToMany( mappedBy = "operator" )
    private List< ProductContact > productContacts;

    @JsonIgnore
    @OneToMany( mappedBy = "operator" )
    private List< Casino > casinos;

    // @JsonIgnore
    // @ManyToOne(targetEntity = com.igs.auto.domain.Casino.class)
    // @JoinColumn(name="id_casino")
    // private List< Casino > casinos = new ArrayList<>( );

}
