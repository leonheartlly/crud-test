package com.igs.auto.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Data
public class Currency implements Serializable {

    @Id
    @Column( length = 4, nullable = false, unique = true)
    public String currency;

    @Column( length = 30, nullable = false )
    public String curr_name;

    @Column( length = 35, nullable = false )
    public String symbol;

    @JsonIgnore
    @ManyToMany( mappedBy = "currencies" )
    private List< Casino > casinos = new ArrayList<>();

    @JsonIgnore
    @ManyToMany( mappedBy = "currencies" )
    private List< Denom > denoms = new ArrayList<>();

    @JsonIgnore
    @OneToMany( mappedBy = "currency" )
    private List< MathConfig > mathConfig;


    public Currency( String currency ) {

        this.currency = currency;
    }


    public Currency() {

    }


    @Override
    public String toString() {

        return currency;
    }
}
