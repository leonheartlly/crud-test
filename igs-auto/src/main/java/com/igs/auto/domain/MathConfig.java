package com.igs.auto.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import org.hibernate.validator.constraints.Length;


@Entity
@Data
public class MathConfig {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( length = 15, nullable = false )
    private String operatorId;

    @Column( length = 4, nullable = false )
    private String casinoId;

    @ManyToOne
    @JoinColumn( name = "currency_id" )
    private Currency currency;

    private String cabinet;

    @ManyToOne
    @JoinColumn( name = "bet_id" )
    private Bet bet;

    @ManyToOne
    @JoinColumn( name = "denom_id" )
    private Denom denom;

    @Column( length = 50, nullable = false, unique = true )
    private String mathIdentifier;
}