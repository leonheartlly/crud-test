package com.igs.auto.utils;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {

        return new Docket( DocumentationType.SWAGGER_2 ).select().apis( RequestHandlerSelectors.basePackage( "com.igs.auto.resources" ) )
            .paths( PathSelectors.any() ).build().apiInfo( apiInfo() );
    }


    private ApiInfo apiInfo() {

        return new ApiInfo( "API IGS Auto", "Cadastro de operadores e cassinos", "Versão 1.0", "", new Contact( "Lucas Ferreira", "", "" ), "", "",
            Collections.emptyList() );

    }


    @Bean
    public CorsFilter corsFilter() {

        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials( true );
        config.setAllowedOrigins( Collections.singletonList( "*" ) );
        config.setAllowedHeaders( Arrays.asList( "Origin", "Content-Type", "Accept" ) );
        config.setAllowedMethods( Arrays.asList( "GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH" ) );
        source.registerCorsConfiguration( "/**", config );
        return new CorsFilter( source );
    }
}
