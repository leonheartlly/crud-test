package com.igs.auto.utils;

import java.text.ParseException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


@Configuration
@Profile( "dev" )
public class DevConfig {

    @Bean
    public boolean instanteateDataBase()
        throws ParseException {

        return true;
    }
}
