package com.igs.auto.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.stream.JsonWriter;
import com.igs.auto.domain.Currency;
import com.igs.auto.domain.Game;

import lombok.Data;


public class FileReaderUtils {

    public static void writeJSONFile() {

        FileWriter file = null;
        try {
            file = new FileWriter( "file_test.json", false );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
        JsonWriter jw = new JsonWriter( file );

    }


    public static void main( String[] args )
        throws IOException {

        byte[] jsonData = Files.readAllBytes( Paths.get( "file_test.json" ) );
        ObjectMapper objectMapper = new ObjectMapper();
        Casino2 oper = objectMapper.readValue( jsonData, Casino2.class);

        objectMapper.configure( SerializationFeature.INDENT_OUTPUT, true );
        StringWriter stringEmp = new StringWriter();

        objectMapper.writeValue( stringEmp, oper );

//        FileWriter file = new FileWriter( "file_test.json" );
//        file.write( stringEmp.toString() );
//        file.close();
        System.out.println( stringEmp.toString() );
    }
}
@Data
class Foda{

	private Operator2 op;

	private List<Casino2> cas;
}

@Data
class Operator2 {

    @JsonIgnore
    private Long id;

    private String name;

    private String operatorId;

    @JsonIgnore
    private boolean isTestingOperator;

    private boolean isIsTestingOperator;

    private boolean saveHistory;

    private boolean retryEnabled;

    private boolean instantDeposit;

    private boolean sendZeroDeposit;

    private String gamePath;

    private String endpoint;

    @JsonUnwrapped
    private List<Casino2> casinos;

}

@Data
 class Casino2 implements Serializable {

    @JsonIgnore
    private Long id;

    private String name;

    private String code;

    private String casinoId;

    private String softlock;

    private boolean reportEnabled;

    private boolean recoverSessionEnabled;

    private String desktopTemplate;

    @JsonIgnore
    private String operatorId;

    private String mobileTemplate;
    @JsonIgnore
    private boolean isCasinoEnabled;
    @JsonIgnore
    private Timestamp casinoLastEnableDate;
    @JsonIgnore
    private Timestamp casinoCreationDate;
    @JsonIgnore
    private String casinoURL;
    @JsonIgnore
    private String casinoLogin;
    @JsonIgnore
    private String casinoPwd;
    @JsonIgnore
    private String apiStagingURL;
    @JsonIgnore
    private String apiProdURL;
    @JsonIgnore
    private List< Currency > currencies = new ArrayList<>();
    @JsonIgnore
    @JsonProperty("validGames")
    private List< Game > games = new ArrayList<>();

}
