package com.igs.auto.utils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;


public class IgsUtils {

    public static int[] convertBet( String data )
        throws Exception {

        String[] dataValues = data.split( "," );
        Integer[] bet = new Integer[ dataValues.length ];

        Integer [] bets = Arrays.asList( data.split( "," ) ).stream().map( x -> Integer.parseInt( x )).toArray(Integer[]::new);

        for ( int i = 0; i < dataValues.length; i++ ) {
            bet[ i ] = Integer.parseInt( dataValues[ i ].trim() );
        }
        if ( !isDataConsistent( bet ) ) {
            throw new Exception( "Cannot repeat numbers on bet" );
        }

        Arrays.sort( bet );
        return ArrayUtils.toPrimitive( bet );

    }


    public static double[] convertDenom( String srtDenom )
        throws Exception {

        String[] denomValues = srtDenom.trim().split( "," );
        Double[] denom = new Double[ denomValues.length ];

        for ( int i = 0; i < denomValues.length; i++ ) {
            denom[ i ] = Double.parseDouble( denomValues[ i ] );
        }

        if ( !isDataConsistent( denom ) ) {
            throw new Exception( "Cannot repeat numbers on denom" );
        }

        Arrays.sort( denom );
        return ArrayUtils.toPrimitive( denom );
    }


    private static boolean isDataConsistent( Number[] data ) {

        Set< Number > valueConsistence = new HashSet<>();
        Arrays.stream( data ).forEach( value -> valueConsistence.add( value ) );
        return valueConsistence.size() == data.length;
    }

}