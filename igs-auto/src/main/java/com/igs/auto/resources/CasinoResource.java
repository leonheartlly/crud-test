package com.igs.auto.resources;


import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.igs.auto.dto.projection.CasinoRgsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.igs.auto.domain.Casino;
import com.igs.auto.dto.CasinoDTO;
import com.igs.auto.dto.CasinoInsertDTO;
import com.igs.auto.dto.OperatorCasinoDTO;
import com.igs.auto.dto.projection.CasinoAccess;
import com.igs.auto.dto.projection.CasinoDetail;
import com.igs.auto.resources.projections.Projection;
import com.igs.auto.service.CasinoService;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@RequestMapping( value = "/casinos" )
public class CasinoResource {

    @Autowired
    private CasinoService casinoService;


    @RequestMapping( method = RequestMethod.GET )
    public ResponseEntity< List< CasinoDTO > > listAll() {

        List< Casino > casinos = casinoService.listAll();
        List< CasinoDTO > casinoDTOS = casinos.stream().map( obj -> new CasinoDTO( obj ) ).collect( Collectors.toList() );

        return ResponseEntity.ok().body( casinoDTOS );
    }


    /**
     * List casino details based on requested object.
     * 
     * @param projection
     *            required projection.
     * @return casino list.
     */
    @GetMapping( value = "/list", produces = { CasinoDetail.ACCEPT_HEADER, CasinoAccess.ACCEPT_HEADER, CasinoRgsRequest.ACCEPT_HEADER } )
    public ResponseEntity listAllProjectedBy( @Projection( def = CasinoDetail.class ) final Class< ? > projection ) {

        List< ? > casinos = casinoService.listAll( projection );

        return ResponseEntity.ok().body( casinos );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > add( @Valid @RequestBody CasinoInsertDTO casinoDTO ) {

        casinoDTO.setId( null );
        Casino casino = casinoService.convertInsertDTO( casinoDTO );

        casino = casinoService.insert( casino );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( casino.getId() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< CasinoDTO > findById( @PathVariable Long id ) {

        Casino casino = casinoService.findById( id );
        CasinoDTO casinoDTO = new CasinoDTO( casino );

        return ResponseEntity.ok().body( casinoDTO );
    }


    @RequestMapping( value = "/{id}/operators", method = RequestMethod.GET )
    public ResponseEntity< List< CasinoDTO > > findByOperatorId( @NotNull @PathVariable String id ) {

        List< Casino > casinos = casinoService.findByOperatorId( id );
        List< CasinoDTO > casinoDTOS = casinos.stream().map( obj -> new CasinoDTO( obj ) ).collect( Collectors.toList() );

        return ResponseEntity.ok().body( casinoDTOS );
    }


    @RequestMapping( value = "name/{name}", method = RequestMethod.GET )
    public ResponseEntity< CasinoDetail > findAPIDataByCasinoId( @NotNull @PathVariable String name ) {

        CasinoDetail casinos = casinoService.findAPIDataByCasinoId( name );

        return ResponseEntity.ok().body( casinos );
    }


    @RequestMapping( value = "enable/list/{enabled}", method = RequestMethod.GET )
    public ResponseEntity< List< OperatorCasinoDTO > > findEnabledCasinos( @NotNull @PathVariable boolean enabled ) {

        List< OperatorCasinoDTO > casinos = casinoService.findEnabledCasinos( enabled );

        return ResponseEntity.ok().body( casinos );
    }


    /**
     * Enables a casino by its name.
     * 
     * @param casinoName
     * @return
     */
    @RequestMapping( value = "enable/{casinoName}/{opId}", method = RequestMethod.POST )
    public ResponseEntity< CasinoDTO > enableCasino( @NotNull @PathVariable String casinoName, @PathVariable( name = "opId" ) String operatorId ) {

        CasinoDTO operatorCasinoDTO = casinoService.enableCasinoByNameAndOperatorId( casinoName, operatorId );

        return ResponseEntity.ok().body( operatorCasinoDTO );
    }

}
