package com.igs.auto.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.igs.auto.domain.Operator;
import com.igs.auto.dto.OperatorDTO;
import com.igs.auto.service.OperatorService;


@RestController
@RequestMapping( value = "/operators" )
public class OperatorResource {

    @Autowired
    private OperatorService operatorService;


    @RequestMapping( method = RequestMethod.GET )
    public ResponseEntity< List< OperatorDTO > > listAll() {

        List< Operator > operators = operatorService.listAll();
        List< OperatorDTO > mathConfigDTOS = operators.stream().map( obj -> new OperatorDTO( obj ) ).collect( Collectors.toList() );

        return ResponseEntity.ok().body( mathConfigDTOS );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > add( @Valid @RequestBody OperatorDTO operatorDTO ) {

        operatorDTO.setId( null );
        Operator mathConfig = operatorService.convertVO( operatorDTO );

        mathConfig = operatorService.insert( mathConfig );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( mathConfig.getId() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< OperatorDTO > findById( @PathVariable Long id ) {

        Operator operator = operatorService.findById( id );
        OperatorDTO mathConfigDTO = new OperatorDTO( operator );

        return ResponseEntity.ok().body( mathConfigDTO );
    }

}
