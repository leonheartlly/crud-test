package com.igs.auto.resources;


import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.igs.auto.domain.Bet;
import com.igs.auto.dto.BetDTO;
import com.igs.auto.service.BetService;


@RestController
@RequestMapping( value = "/bets" )
public class BetResource {

    @Autowired
    private BetService betService;


    @RequestMapping( method = RequestMethod.GET )
    public ResponseEntity< List< BetDTO > > listAll() {

        List< Bet > bets = betService.listAll();
        List< BetDTO > betDTOS = bets.stream().map( obj -> new BetDTO( obj ) ).collect( Collectors.toList() );

        return ResponseEntity.ok().body( betDTOS );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > add( @Valid @RequestBody BetDTO betDTO ) {

        betDTO.setId( null );
        Bet bet = betService.convertDTO( betDTO );

        bet = betService.insert( bet );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( bet.getId() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< BetDTO > findById( @PathVariable Long id )
        throws Exception {

        Bet bet = betService.findById( id );
        BetDTO betDTO = new BetDTO( bet );

        return ResponseEntity.ok().body( betDTO );
    }

}
