package com.igs.auto.resources;


import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.igs.auto.domain.Game;
import com.igs.auto.dto.GameDTO;
import com.igs.auto.service.GameService;


@RestController
@RequestMapping( value = "/games" )
public class GameResource {

    @Autowired
    private GameService gameService;


    @RequestMapping( method = RequestMethod.GET )
    public ResponseEntity< List< GameDTO > > listAll() {

        List< Game > games = gameService.listAll();
        List< GameDTO > gameDTOS = games.stream().map( obj -> new GameDTO( obj ) ).collect( Collectors.toList() );

        return ResponseEntity.ok().body( gameDTOS );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > add( @Valid @RequestBody GameDTO gameDTO ) {

        gameDTO.setId( null );
        Game game = gameService.convertVO( gameDTO );

        game = gameService.insert( game );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( game.getId() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< GameDTO > findById( @PathVariable Long id ) {

        Game game = gameService.findById( id );
        GameDTO gameDTO = new GameDTO( game );

        return ResponseEntity.ok().body( gameDTO );
    }


    @RequestMapping( value = "ortiz/{id}", method = RequestMethod.GET )
    public ResponseEntity< GameDTO > findByGameId( @PathVariable String id ) {

        Game game = gameService.findByGameId( id );
        GameDTO gameDTO = new GameDTO( game );

        return ResponseEntity.ok().body( gameDTO );
    }

}
