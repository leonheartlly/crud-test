package com.igs.auto.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.igs.auto.domain.Denom;
import com.igs.auto.dto.DenomDTO;
import com.igs.auto.dto.DenomInsertDTO;
import com.igs.auto.service.DenomService;


@RestController
@RequestMapping( value = "/denoms" )
public class DenomResource {

    @Autowired
    private DenomService denomService;


    @RequestMapping( method = RequestMethod.GET )
    public ResponseEntity< List< DenomDTO > > listAll() {

        List< Denom > denoms = denomService.listAll();
        List< DenomDTO > denomDTOS = denoms.stream().map( obj -> new DenomDTO( obj ) ).collect( Collectors.toList() );

        return ResponseEntity.ok().body( denomDTOS );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > add( @Valid @RequestBody DenomInsertDTO denomDTO ) {

//        denomDTO.setId( null );
        Denom denom = denomService.convertInsertDTO( denomDTO );

        denom = denomService.insert( denom );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( denom.getId() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< DenomDTO > findById( @PathVariable Long id )
        throws Exception {

        Denom denom = denomService.findById( id );
        DenomDTO denomDTO = new DenomDTO( denom );

        return ResponseEntity.ok().body( denomDTO );
    }
}
