package com.igs.auto.resources;


import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.igs.auto.dto.ProductContactInsertDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.igs.auto.domain.ProductContact;
import com.igs.auto.dto.ProductContactDTO;
import com.igs.auto.service.ProductContactService;


@RestController
@RequestMapping( value = "/product/supports" )
public class ProductContactResource {

    @Autowired
    private ProductContactService casinoService;


    /***
     * List all contacts from casino records.
     * 
     * @return List casino DTOS.
     */
    @RequestMapping( method = RequestMethod.GET )
    public ResponseEntity< List< ProductContactDTO > > listAll() {

        List< ProductContact > productContacts = casinoService.listAll();
        List< ProductContactDTO > productContactDTOS = productContacts.stream().map( obj -> new ProductContactDTO( obj ) ).collect( Collectors.toList() );

        return ResponseEntity.ok().body( productContactDTOS );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > add( @Valid @RequestBody ProductContactInsertDTO productContactInsertDTO ) {

        ProductContact productContact = casinoService.convertNewDTO( productContactInsertDTO );

        productContact = casinoService.insert( productContact );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( productContact.getId() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< ProductContactDTO > findById( @Valid @PathVariable Long id ) {

        ProductContact productContact = casinoService.findById( id );
        ProductContactDTO casinoDTO = new ProductContactDTO( productContact );

        return ResponseEntity.ok().body( casinoDTO );
    }


    @RequestMapping( value = "operator/{id}", method = RequestMethod.GET )
    public ResponseEntity< List< ProductContactDTO > > findByOperatorId( @NotBlank @PathVariable String id ) {

        List< ProductContactDTO > productContact = casinoService.findByProductId( id );

        return ResponseEntity.ok().body( productContact );
    }

    @RequestMapping( value = "operator/{opid}/{area}", method = RequestMethod.GET )
    public ResponseEntity< List< ProductContactDTO > > findByOperatorId( @NotBlank @PathVariable(name = "opid") String operatorId, String area ) {

        List< ProductContactDTO > productContact = casinoService.findByOperatorIdAndArea( operatorId, area );

        return ResponseEntity.ok().body( productContact );
    }

}
