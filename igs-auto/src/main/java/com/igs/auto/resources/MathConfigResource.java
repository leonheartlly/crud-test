package com.igs.auto.resources;


import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.igs.auto.domain.MathConfig;
import com.igs.auto.dto.MathConfigDTO;
import com.igs.auto.dto.MathConfigInsertDTO;
import com.igs.auto.service.MathConfigService;


@RestController
@RequestMapping( value = "/mathconfigs" )
public class MathConfigResource {

    @Autowired
    private MathConfigService mathConfigService;


    @RequestMapping( method = RequestMethod.GET )
    public ResponseEntity< List< MathConfigDTO > > listAll() {

        List< MathConfig > mathConfigs = mathConfigService.listAll();
        List< MathConfigDTO > mathConfigDTOS = mathConfigs.stream().map( obj -> new MathConfigDTO( obj ) ).collect( Collectors.toList() );

        return ResponseEntity.ok().body( mathConfigDTOS );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > add( @Valid @RequestBody MathConfigInsertDTO mathConfigDTO ) {

        MathConfig mathConfig = mathConfigService.convertInsertDTO( mathConfigDTO );

        mathConfig = mathConfigService.insert( mathConfig );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( mathConfig.getId() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< MathConfigDTO > findById( @PathVariable Long id ) {

        MathConfig mathConfig = mathConfigService.findById( id );
        MathConfigDTO mathConfigDTO = new MathConfigDTO( mathConfig );

        return ResponseEntity.ok().body( mathConfigDTO );
    }

}
