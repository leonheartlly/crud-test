package com.igs.auto.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.igs.auto.domain.Currency;
import com.igs.auto.dto.CurrencyDTO;
import com.igs.auto.service.CurrencyService;


@RestController
@RequestMapping( value = "/currencies" )
public class CurrencyResource {

    @Autowired
    private CurrencyService currencyService;


    @RequestMapping( method = RequestMethod.GET )
    public ResponseEntity< List< CurrencyDTO > > listAll() {

        List< Currency > currencies = currencyService.listAll();
        List< CurrencyDTO > currencyDTOS = currencies.stream().map( obj -> new CurrencyDTO( obj ) ).collect( Collectors.toList() );

        return ResponseEntity.ok().body( currencyDTOS );
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity< Void > add( @Valid @RequestBody CurrencyDTO currencyDTO ) {

        Currency currency = currencyService.convertVO( currencyDTO );

        currency = currencyService.insert( currency );

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path( "/{id}" ).buildAndExpand( currency.getCurrency() ).toUri();

        return ResponseEntity.created( uri ).build();
    }


    @RequestMapping( value = "/{key}", method = RequestMethod.GET )
    public ResponseEntity< CurrencyDTO > findById( @PathVariable String key ) {

        Currency currency = currencyService.findById( key );
        CurrencyDTO currencyDTO = new CurrencyDTO( currency );

        return ResponseEntity.ok().body( currencyDTO );
    }
}
