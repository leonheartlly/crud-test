package com.igs.auto.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

import com.igs.auto.domain.Bet;
import com.igs.auto.domain.Currency;
import com.igs.auto.domain.Denom;
import com.igs.auto.domain.MathConfig;
import com.igs.auto.dto.MathConfigDTO;
import com.igs.auto.dto.MathConfigInsertDTO;
import com.igs.auto.repository.BetRepository;
import com.igs.auto.repository.CurrencyRepository;
import com.igs.auto.repository.DenomRepository;
import com.igs.auto.repository.MathConfigRepository;
import com.igs.auto.service.Exceptions.DataIntegrityException;
import com.igs.auto.service.Exceptions.ObjectNotFoundException;

@Slf4j
@Service
public class MathConfigService {

    @Autowired
    private MathConfigRepository mathConfigRepository;

    @Autowired
    private BetRepository betRepository;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private DenomRepository denomRepository;


    public MathConfig findById( Long id ) {

        Optional< MathConfig > city = mathConfigRepository.findById( id );

        return city.orElseThrow(
            () -> new ObjectNotFoundException( "The object you are looking for does not exist. Id: " + id + ", Type: " + MathConfig.class.getName() ) );
    }


    public List< MathConfig > listAll() {

        List< MathConfig > mathConfigs = new ArrayList<>();
        try {
            mathConfigs = mathConfigRepository.findAll();

        } catch ( EntityNotFoundException enf ) {
            log.error( "[MCLA] ", enf );
            throw new DataIntegrityException( "[MCLA] Could not find a requested value. ", enf );
        } catch ( JpaObjectRetrievalFailureException e ) {
            log.error( "[MCLA] Code: " + e.hashCode(), e );
            throw new DataIntegrityException( "[MCLA] Could not find a requested value. Code: " + e.hashCode(), e );
        } catch ( Exception e ) {
            log.error( "[MCLA] Code: " + e.hashCode(), e );
            throw new DataIntegrityException( "[MCLA] Could not find a requested value. ", e );
        }

        return mathConfigs;
    }


    public MathConfig insert( MathConfig mathConfig ) {

        mathConfig.setId( null );
        MathConfig math;

        try {
            math = mathConfigRepository.save( mathConfig );
            log.info( "[MCI] New MathConfig: " + mathConfig.getMathIdentifier() );
        } catch ( IllegalArgumentException ia ) {
            log.error( "[MCI] MathConfig: " + mathConfig.getMathIdentifier() + " Code: " + ia.hashCode(), ia );
            throw new DataIntegrityException( "[MCI] Could not insert value. Code: " + ia.hashCode(), ia );
        } catch ( Exception e ) {
            log.error( "[MCI] MathConfig: " + mathConfig.getMathIdentifier() + " Code: " + e.hashCode(), e );
            throw new DataIntegrityException( "[MCI] Could not insert value. Code: " + e.hashCode(), e );
        }

        return math;
    }


    public MathConfig convertVO( MathConfigDTO mathConfigDTO ) {

        MathConfig mathConfig = new MathConfig();
        mathConfig.setId( mathConfigDTO.getId() );
        mathConfig.setCabinet( mathConfigDTO.getCabinet() );
        mathConfig.setCasinoId( mathConfigDTO.getCasinoId() );
        mathConfig.setOperatorId( mathConfigDTO.getOperatorId() );

        return mathConfig;
    }


    public MathConfig convertInsertDTO( MathConfigInsertDTO mathConfigDTO ) {

        MathConfig mathConfig = new MathConfig();

        mathConfig.setCabinet( mathConfigDTO.getCabinet() );
        mathConfig.setCasinoId( mathConfigDTO.getCasinoId() );
        mathConfig.setOperatorId( mathConfigDTO.getOperatorId() );

        Optional< Bet > bet = betRepository.findById( mathConfigDTO.getBetId() );
        mathConfig.setBet( bet.orElseThrow( () -> new ObjectNotFoundException( "Required bet not found." ) ) );

        Optional< Currency > cur = currencyRepository.findById( mathConfigDTO.getCurrency() );
        mathConfig.setCurrency( cur.orElseThrow( () -> new ObjectNotFoundException( "Required currency not found." ) ) );

        Optional< Denom > denom = denomRepository.findById( mathConfigDTO.getDenom() );
        mathConfig.setDenom( denom.orElseThrow( () -> new ObjectNotFoundException( "Required denom not found." ) ) );

        mathConfig.setMathIdentifier( mathConfigDTO.generateMathIdentifier() );

        return mathConfig;
    }

}
