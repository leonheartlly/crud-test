package com.igs.auto.service;


import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igs.auto.domain.Bet;
import com.igs.auto.dto.BetDTO;
import com.igs.auto.repository.BetRepository;
import com.igs.auto.service.Exceptions.ObjectNotFoundException;


@Service
public class BetService {

    @Autowired
    private BetRepository betRepository;


    public Bet findById( Long id ) {

        Optional< Bet > city = betRepository.findById( id );

        return city
            .orElseThrow( () -> new ObjectNotFoundException( "The object you are looking for does not exist. Id: " + id + ", Type: " + Bet.class.getName() ) );
    }


    public List< Bet > listAll() {

        List< Bet > bets = betRepository.findAll();
        return bets;
    }


    // TODO criar excessão personalizada
    public Bet insert( Bet bet ) {

        bet.setId( null );

        Optional< Bet > optBet = betRepository.findOneByBets( bet.getBets() );

        if ( !optBet.isPresent() ) {

            return betRepository.save( bet );
        }

        return optBet.get();
    }


    public Bet convertDTO( BetDTO betDTO ) {

        Bet bet = new Bet();
        bet.setId( betDTO.getId() );
        bet.setNickname( betDTO.getNickname() );
        bet.setBets( Arrays.toString( betDTO.getBets() ).replace( "[", "" ).replace( "]", "" ) );

        return bet;
    }


    private boolean checkNumericValue( String[] betValues ) {

        boolean isNumeric = Arrays.stream( betValues ).noneMatch( t -> !NumberUtils.isDigits( t ) );
        return isNumeric;
    }
}
