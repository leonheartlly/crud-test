package com.igs.auto.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.igs.auto.domain.Casino;
import com.igs.auto.domain.Operator;
import com.igs.auto.domain.ProductContact;
import com.igs.auto.dto.ProductContactInsertDTO;
import com.igs.auto.dto.ProductContactDTO;
import com.igs.auto.repository.CasinoRepository;
import com.igs.auto.repository.OperatorRepository;
import com.igs.auto.repository.ProductContactRepository;
import com.igs.auto.service.Exceptions.ObjectNotFoundException;


@Service
public class ProductContactService {

    @Autowired
    private ProductContactRepository productContactRepository;

    @Lazy
    @Autowired
    private CasinoRepository casinoRepository;

    @Lazy
    @Autowired
    private OperatorRepository operatorRepository;


    public ProductContact findById( Long id ) {

        Optional< ProductContact > casinoSupport = productContactRepository.findById( id );

        return casinoSupport.orElseThrow(
            () -> new ObjectNotFoundException( "The object you are looking for does not exist. Id: " + id + ", Type: " + ProductContact.class.getSimpleName() ) );
    }


    public List< ProductContact > listAll() {

        List< ProductContact > productContacts = productContactRepository.findAll();
        return productContacts;
    }


    /**
     * Find contacts by its operator;
     * 
     * @param id
     *            operator.
     * @return contact list.
     */
    public List< ProductContactDTO > findByProductId( String id ) {

        List< ProductContactDTO > productContactDTOS = productContactRepository.findByOperatorOperatorId( id, ProductContactDTO.class );

        return productContactDTOS;
    }


    /**
     * Look for contacts by its operator and area.
     * 
     * @param operatorId
     *            operator.
     * @param area
     *            working area.
     * @return contact list.
     */
    public List< ProductContactDTO > findByOperatorIdAndArea( String operatorId, String area ) {

        List< ProductContactDTO > productContactDTOS = productContactRepository.findByOperatorOperatorIdAndArea( operatorId, area, ProductContactDTO.class );

        return productContactDTOS;
    }


    @Transactional
    public ProductContact insert( ProductContact productContact ) {

        productContact.setId( null );
        return productContactRepository.save( productContact );
    }


    public ProductContact convertDTO( ProductContactDTO productContactDTO ) {

        ProductContact productContact = new ProductContact();
        productContact.setSkypeGroup( productContactDTO.getSkypeGroup() );
        productContact.setArea( productContactDTO.getArea() );
        productContact.setContactName( productContactDTO.getContactName() );
        productContact.setEmail( productContactDTO.getEmail() );
        productContact.setCasino( productContactDTO.getCasino().convertCasino() );

        return productContact;
    }


    public ProductContact convertNewDTO( ProductContactInsertDTO productContactInsertDTO ) {

        ProductContact productContact = new ProductContact();
        productContact.setContactName( productContactInsertDTO.getContactName() );
        productContact.setArea( productContactInsertDTO.getArea() );
        productContact.setEmail( productContactInsertDTO.getEmail() );
        productContact.setSkypeGroup( productContactInsertDTO.getSkypeGroup() );

        if ( productContactInsertDTO.getCasinoid() > 0 ) {

            Optional< Casino > optCasino = casinoRepository.findById( productContactInsertDTO.getCasinoid());
            productContact.setCasino( optCasino.orElseThrow( () -> new ObjectNotFoundException( "Required casino not found." ) ) );
        } else {
            Optional< Operator > optOperator = operatorRepository.findByOperatorId( productContactInsertDTO.getOperatorId() );
            productContact.setOperator( optOperator.orElseThrow( () -> new ObjectNotFoundException( "Required operator not found." ) ) );
        }

        return productContact;
    }
}
