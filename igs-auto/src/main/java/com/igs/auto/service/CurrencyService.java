package com.igs.auto.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igs.auto.domain.Currency;
import com.igs.auto.dto.CurrencyDTO;
import com.igs.auto.repository.CurrencyRepository;
import com.igs.auto.service.Exceptions.ObjectNotFoundException;


@Service
public class CurrencyService {

    @Autowired
    private CurrencyRepository currencyRepository;


    public Currency findById( String key ) {

        Optional< Currency > city = currencyRepository.findById( key );

        return city.orElseThrow(
            () -> new ObjectNotFoundException( "The object you are looking for does not exist. Id: " + key + ", Type: " + Currency.class.getName() ) );
    }


    public List< Currency > listAll() {

        List< Currency > Currencys = currencyRepository.findAll();
        return Currencys;
    }


    public Currency insert( Currency Currency ) {

        // Currency.setId( null );
        return currencyRepository.save( Currency );
    }

    /**
     * Converts a DTO to domain object.
     * @param currencyDTO currency dto.
     * @return Currency.
     */
    public Currency convertVO( CurrencyDTO currencyDTO ) {

        Currency currency = new Currency();
        currency.setCurrency( currencyDTO.getCurrency().toUpperCase() );
        currency.setCurr_name( currencyDTO.getCurr_name() );
        currency.setSymbol( currencyDTO.getSymbol() );

        return currency;
    }

}
