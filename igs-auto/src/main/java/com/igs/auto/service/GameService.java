package com.igs.auto.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igs.auto.domain.Game;
import com.igs.auto.dto.GameDTO;
import com.igs.auto.repository.GameRepository;
import com.igs.auto.service.Exceptions.ObjectNotFoundException;


@Service
public class GameService {

    @Autowired
    private GameRepository gameRepository;


    public Game findById( Long id ) {

        Optional< Game > city = gameRepository.findById( id );

        return city
            .orElseThrow( () -> new ObjectNotFoundException( "The object you are looking for does not exist. Id: " + id + ", Type: " + Game.class.getName() ) );
    }

    public Game findByGameId( String id ) {

        Optional< Game > city = gameRepository.findOneByGameId( id );

        return city
                .orElseThrow( () -> new ObjectNotFoundException( "The object you are looking for does not exist. Id: " + id + ", Type: " + Game.class.getName() ) );
    }


    public List< Game > listAll() {

        List< Game > bets = gameRepository.findAll();
        return bets;
    }


    public Game insert( Game bet ) {

        bet.setId( null );
        return gameRepository.save( bet );
    }


    public Game convertVO( GameDTO gameDTO ) {

        Game game = new Game();
        game.setId( gameDTO.getId() );
        game.setGameType( gameDTO.getGameType() );
        game.setGame( gameDTO.getGame() );
        game.setGameId( gameDTO.getGameId() );
        game.setOrtizNetModelNumber( gameDTO.getOrtizNetModelNumber() );
        // game.setMathConfig( gameDTO.getMathConfig() );

        return game;
    }

}
