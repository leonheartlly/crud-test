package com.igs.auto.service;


import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.igs.auto.domain.Casino;
import com.igs.auto.domain.Currency;
import com.igs.auto.domain.Game;
import com.igs.auto.domain.Operator;
import com.igs.auto.dto.CasinoDTO;
import com.igs.auto.dto.projection.CasinoDetail;
import com.igs.auto.dto.CasinoInsertDTO;
import com.igs.auto.dto.CurrencyDTO;
import com.igs.auto.dto.OperatorCasinoDTO;
import com.igs.auto.repository.CasinoRepository;
import com.igs.auto.repository.CurrencyRepository;
import com.igs.auto.repository.GameRepository;
import com.igs.auto.repository.OperatorRepository;
import com.igs.auto.service.Exceptions.ObjectNotFoundException;


@Service
public class CasinoService {

    private final Logger log = LoggerFactory.getLogger( CasinoService.class );

    @Autowired
    private CasinoRepository casinoRepository;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private GameRepository gameRepository;


    public Casino findById( Long id ) {

        Optional< Casino > casino = casinoRepository.findById( id );

        return casino.orElseThrow(
            () -> new ObjectNotFoundException( "The object you are looking for does not exist. Id: " + id + ", Type: " + Casino.class.getName() ) );
    }


    public List< Casino > listAll() {

        List< Casino > casinos = casinoRepository.findAll();
        return casinos;
    }


    public List< ? > listAll( Class< ? > projection ) {

        List< ? > casinos = casinoRepository.findAllProjectedBy( projection );
        return casinos;
    }


    /**
     * Find casino list by operator id.
     * 
     * @param operatorId
     *            operator id;
     * @return casino list.
     */
    public List< Casino > findByOperatorId( String operatorId ) {

        List< Casino > casinos = casinoRepository.findByOperatorOperatorId( operatorId );
        return casinos;
    }


    public CasinoDetail findAPIDataByCasinoId( String casinoId ) {

        CasinoDetail casino = casinoRepository.findByName( casinoId );
        return casino;
    }


    /**
     * List all enabled and disabled casinos.
     * 
     * @param isEnabled
     *            enabled or disabled.
     * @return OperatorCasinoDTO list.
     */
    public List< OperatorCasinoDTO > findEnabledCasinos( boolean isEnabled ) {

        List< OperatorCasinoDTO > operatorCasinoDTOS = casinoRepository.findByIsCasinoEnabled( isEnabled, OperatorCasinoDTO.class );
        return operatorCasinoDTOS;
    }


    /**
     * Enables a casino by its name and operatorId.
     * 
     * @param casinoName
     *            casino name.
     * @param operatorId
     *            operator id in which casino is working.
     * @return updated casino.
     */
    public CasinoDTO enableCasinoByNameAndOperatorId( String casinoName, String operatorId ) {

        CasinoDTO operatorCasinoDTO = casinoRepository.findByNameAndOperatorOperatorId( casinoName, operatorId, CasinoDTO.class );

        Casino casino = convertDTO( operatorCasinoDTO );
        casino.setCasinoEnabled( true );
        casino.setCasinoLastEnableDate( new Timestamp( Instant.now().toEpochMilli() ) );

        casinoRepository.save( casino );
        return operatorCasinoDTO;
    }


    @Transactional
    public Casino insert( Casino casino ) {

        casino.setId( null );
        casino.setCasinoEnabled( false );
        casino.setCasinoCreationDate( new Timestamp( Instant.now().toEpochMilli() ) );

        return casinoRepository.save( casino );
    }


    public Casino convertDTO( CasinoDTO casinoDTO ) {

        Casino casino = new Casino();

        Optional< Operator > operator = operatorRepository.findByOperatorId( casinoDTO.getOperatorId() );
        casino.setOperator( operator.orElseThrow( () -> new ObjectNotFoundException( "Required required operator not found." ) ) );

        casino.setId( casinoDTO.getId() );
        casino.setName( casinoDTO.getName() );
        casino.setCode( casinoDTO.getCode() );
        casino.setCasinoId( casinoDTO.getCasinoId() );
        casino.setSoftlock( casinoDTO.getSoftlock() );
        casino.setReportEnabled( casinoDTO.isReportEnabled() );
        casino.setRecoverSessionEnabled( casinoDTO.isRecoverSessionEnabled() );
        casino.setDesktopTemplate( casinoDTO.getDesktopTemplate() );
        casino.setMobileTemplate( casinoDTO.getMobileTemplate() );

        casino.setApiProdURL( casinoDTO.getApiProdURL() );
        casino.setApiStagingURL( casinoDTO.getApiStagingURL() );
        casino.setCasinoCreationDate( casinoDTO.getCasinoCreationDate() );
        casino.setCasinoPwd( casinoDTO.getCasinoPwd() );
        casino.setCasinoLogin( casinoDTO.getCasinoLogin() );
        casino.setCasinoURL( casinoDTO.getCasinoURL() );

        // TODO trycatch
        final List< String > collect = casinoDTO.getCurrencies().stream().map( CurrencyDTO::toString ).collect( Collectors.toList() );
        List< Currency > currencies = currencyRepository.findAllById( collect );

        casino.setCurrencies( currencies );

        return casino;
    }


    public Casino convertInsertDTO( CasinoInsertDTO casinoDTO ) {

        Casino casino = new Casino();

        Optional< Operator > optOperator = operatorRepository.findByOperatorId( casinoDTO.getOperatorId() );
        casino.setOperator( optOperator.orElseThrow( () -> new ObjectNotFoundException( "Required required operator not found." ) ) );

        List< Game > games = gameRepository.findAllByGameIdIn( casinoDTO.getValidGames(), Game.class );
        if ( games.size() != casinoDTO.getValidGames().size() ) {
            log.warn(
                "[CSI] Could not find one or more required games. Will not insert data. Games: " + casinoDTO.getValidGames() + " Casino: "
                    + casinoDTO.getName() );
            throw new ObjectNotFoundException( "[CSI] Could not one or more required games. Will not insert data." );
        }
        casino.setGames( games );

        casino.setId( casinoDTO.getId() );
        casino.setName( casinoDTO.getName() );
        casino.setCode( casinoDTO.getCode() );
        casino.setCasinoId( casinoDTO.getCasinoId() );
        casino.setSoftlock( casinoDTO.getSoftlock() );
        casino.setReportEnabled( casinoDTO.isReportEnabled() );
        casino.setRecoverSessionEnabled( casinoDTO.isRecoverSessionEnabled() );
        casino.setDesktopTemplate( casinoDTO.getDesktopTemplate() );
        casino.setMobileTemplate( casinoDTO.getMobileTemplate() );
        casino.setCasinoURL( casinoDTO.getCasinoURL() );
        casino.setApiProdURL( casinoDTO.getApiProdURL() );
        casino.setApiStagingURL( casinoDTO.getApiStagingURL() );
        casino.setCasinoPwd( casinoDTO.getCasinoPwd() );
        casino.setCasinoLogin( casinoDTO.getCasinoLogin() );

        List< Currency > currencies = currencyRepository.findAllById( casinoDTO.getCurrencies() );
        if ( currencies.size() != casinoDTO.getCurrencies().size() ) {
            log.error( "[CSI] Could not find one or more required currencies. Will not insert data." );
            throw new ObjectNotFoundException( "A currency you want is not registered." );
        }

        casino.setCurrencies( currencies );

        return casino;
    }

}
