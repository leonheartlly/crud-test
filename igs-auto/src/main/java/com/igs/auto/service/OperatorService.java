package com.igs.auto.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igs.auto.domain.Operator;
import com.igs.auto.dto.OperatorDTO;
import com.igs.auto.repository.OperatorRepository;
import com.igs.auto.service.Exceptions.ObjectNotFoundException;


@Service
public class OperatorService {

    @Autowired
    private OperatorRepository operatorRepository;


    public Operator findById( Long id ) {

        Optional< Operator > operator = operatorRepository.findById( id );

        return operator.orElseThrow(
            () -> new ObjectNotFoundException( "The object you are looking for does not exist. Id: " + id + ", Type: " + Operator.class.getName() ) );
    }


    public List< Operator > listAll() {

        List< Operator > operators = operatorRepository.findAll();
        return operators;
    }


    public Operator insert( Operator operator ) {

        operator.setId( null );
        return operatorRepository.save( operator );
    }


    public Operator convertVO( OperatorDTO operatorDTO ) {

        Operator operator = new Operator();
        operator.setId( operatorDTO.getId() );
        operator.setName( operatorDTO.getName() );
        operator.setOperatorId( operatorDTO.getOperatorId() );
        operator.setSaveHistory( operatorDTO.isSaveHistory() );
        operator.setInstantDeposit( operatorDTO.isInstantDeposit() );
        operator.setTestingOperator( operatorDTO.isTestingOperator() );
        operator.setSendZeroDeposit( operatorDTO.isSendZeroDeposit() );
        operator.setRetryEnabled( operatorDTO.isRetryEnabled() );
        operator.setEndpoint( operatorDTO.getEndpoint() );
        operator.setGamePath( operatorDTO.getGamePath() );

        return operator;
    }
}
