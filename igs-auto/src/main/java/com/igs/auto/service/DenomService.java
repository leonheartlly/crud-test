package com.igs.auto.service;


import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.igs.auto.domain.Currency;
import com.igs.auto.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igs.auto.domain.Denom;
import com.igs.auto.dto.DenomDTO;
import com.igs.auto.dto.DenomInsertDTO;
import com.igs.auto.repository.DenomRepository;
import com.igs.auto.service.Exceptions.ObjectNotFoundException;


@Service
public class DenomService {

    @Autowired
    private DenomRepository denomRepository;

    @Autowired
    private CurrencyRepository currencyRepository;


    public Denom findById( Long id ) {

        Optional< Denom > city = denomRepository.findById( id );

        return city.orElseThrow(
            () -> new ObjectNotFoundException( "The object you are looking for does not exist. Id: " + id + ", Type: " + Denom.class.getName() ) );
    }


    public List< Denom > listAll() {

        List< Denom > denoms = denomRepository.findAll();
        return denoms;
    }


    public Denom insert( Denom denom ) {

        denom.setId( null );
        return denomRepository.save( denom );
    }


    public Denom convertDTO( DenomDTO denomDTO ) {

        Denom denom = new Denom();
        denom.setId( denomDTO.getId() );
        denom.setDenoms( Arrays.toString( denomDTO.getDenom() ).replace( "[", "" ).replace( "]", "" ) );
        // denom.getCurrencies(denomDTO.getCurrencies());

        return denom;
    }


    public Denom convertInsertDTO( DenomInsertDTO denomDTO ) {

        Denom denom = new Denom();
        Optional< Currency > currency = currencyRepository.findById( denomDTO.getCurrency().trim().toUpperCase() );
        denom.getCurrencies()
            .add( currency.orElseThrow( () -> new ObjectNotFoundException( "[ONF1] Could not find desired currency. Currency: " + denomDTO.getCurrency() ) ) );

        denom.setId( denomDTO.getId() );
        denom.setDenoms( Arrays.toString( denomDTO.getDenom() ).replace( "[", "" ).replace( "]", "" ) );

        return denom;
    }

}
