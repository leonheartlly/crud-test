package com.igs.auto.service.validator;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.igs.auto.domain.Casino;
import com.igs.auto.domain.Operator;
import com.igs.auto.dto.ProductContactInsertDTO;
import com.igs.auto.repository.CasinoRepository;
import com.igs.auto.repository.OperatorRepository;
import com.igs.auto.resources.exceptions.FieldMessage;


public class ContactInsertValidator implements ConstraintValidator< ContactInsert, ProductContactInsertDTO > {

    @Autowired
    private CasinoRepository casinoRepository;

    @Autowired
    private OperatorRepository operatorRepository;


    @Override
    public void initialize( ContactInsert ann ) {

    }

    /**
     * Custom validations to casino and operator.
     * @param dto new product contact.
     * @param context
     * @return is valid.
     */
    @Override
    public boolean isValid( ProductContactInsertDTO dto, ConstraintValidatorContext context ) {

        List< FieldMessage > invalidList = new ArrayList<>();

        if ( dto.getCasinoid() > 0 ) {
            Optional< Casino > aux = casinoRepository.findById( dto.getCasinoid() );
            if ( !aux.isPresent() ) {
                invalidList.add( new FieldMessage( "casinoid", "Required casino is not valid." ) );
            }
        } else {
            if ( StringUtils.isNotBlank( dto.getOperatorId() ) ) {
                Optional< Operator > aux = operatorRepository.findByOperatorId( dto.getOperatorId() );
                if ( !aux.isPresent() ) {
                    invalidList.add( new FieldMessage( "operatorId", "Required operator is not valid." ) );
                }
            } else {
                invalidList.add( new FieldMessage( "operatorId", "You need to link a contact either with a casino or a operator. Both cannot be empty." ) );
            }

        }

        for ( FieldMessage e : invalidList ) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate( e.getMessage() ).addPropertyNode( e.getFieldName() ).addConstraintViolation();
        }

        return invalidList.isEmpty();
    }
}
