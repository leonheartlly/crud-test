package com.igs.auto.service.validator;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.igs.auto.domain.Currency;
import com.igs.auto.dto.DenomInsertDTO;
import com.igs.auto.repository.CurrencyRepository;
import com.igs.auto.resources.exceptions.FieldMessage;


public class DenomInsertValidator implements ConstraintValidator< GenerictInsert, DenomInsertDTO > {

    @Autowired
    private CurrencyRepository currencyRepository;


    @Override
    public void initialize( GenerictInsert ann ) {

    }


    /**
     * Custom validations to .
     * 
     * @param dto
     *            new product contact.
     * @param context
     * @return is valid.
     */
    @Override
    public boolean isValid( DenomInsertDTO dto, ConstraintValidatorContext context ) {

        List< FieldMessage > invalidList = new ArrayList<>();

        Optional< Currency > aux = currencyRepository.findById( dto.getCurrency() );

        if ( !aux.isPresent() ) {
            invalidList.add( new FieldMessage( "currency", "A valid currency is required." ) );
        }

        for ( FieldMessage e : invalidList ) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate( e.getMessage() ).addPropertyNode( e.getFieldName() ).addConstraintViolation();
        }

        return invalidList.isEmpty();
    }

}
