package com.igs.auto.dto.projection;

import java.util.List;


/**
 * Retrieve {@link com.igs.auto.domain.Casino} information for RGS request.
 */
public interface CasinoRgsRequest {

    String ACCEPT_HEADER = "application/vnd.casino-rgs-request+json";


    String getName();


    String getCode();


    String getCasinoId();


    String getSoftlock();


    String IsReportEnabled();


    String IsRecoverSessionEnabled();


    String getDesktopTemplate();


    String getMobileTemplate();


    List< CurrencyRgs > getCurrencies();

    /**
     * Interface to retrieve only the needed values from
     * {@link com.igs.auto.domain.Currency}.
     */
    interface CurrencyRgs {

        String getCurrency();
    }


    List< GameId > getGames();

    /**
     * Interface to retrieve only the needed values from
     * {@link com.igs.auto.domain.Game}.
     */
    interface GameId {

        String getGameId();
    }

}
