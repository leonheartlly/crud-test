package com.igs.auto.dto;


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.igs.auto.domain.Casino;
import com.igs.auto.domain.Game;

import com.igs.auto.domain.ProductContact;
import lombok.Data;
import org.apache.commons.lang3.ObjectUtils;


@Data
public class CasinoDTO implements Serializable {

    @JsonIgnore
    private Long id;

    private String name;

    private String code;

    private String casinoId;

    private String softlock;

    private boolean reportEnabled;

    private boolean recoverSessionEnabled;

    private String desktopTemplate;

    private String mobileTemplate;

    private String casinoURL;

    private String casinoLogin;

    private String casinoPwd;

    private String apiStagingURL;

    private String apiProdURL;

    private Timestamp casinoCreationDate;

    private Timestamp casinoLastEnableDate;

    private boolean isCasinoEnabled;

    private List< CurrencyDTO > currencies = new ArrayList<>();

    private List< String > validGames = new ArrayList<>();

    private List< ProductContactDTO > productContacts;

    private String operatorId;


    public CasinoDTO() {

    }


    public CasinoDTO( Casino casino ) {

        if ( ObjectUtils.isNotEmpty( casino ) ) {
            this.id = casino.getId();
            this.name = casino.getName();
            this.code = casino.getCode();
            this.casinoId = casino.getCasinoId();
            this.softlock = casino.getSoftlock();
            this.reportEnabled = casino.isReportEnabled();
            this.recoverSessionEnabled = casino.isRecoverSessionEnabled();
            this.desktopTemplate = casino.getDesktopTemplate();
            this.mobileTemplate = casino.getMobileTemplate();
            this.casinoPwd = casino.getCasinoPwd();
            this.casinoLogin = casino.getCasinoLogin();
            this.casinoLastEnableDate = casino.getCasinoLastEnableDate();
            this.casinoCreationDate = casino.getCasinoCreationDate();
            this.apiProdURL = casino.getApiProdURL();
            this.apiStagingURL = casino.getApiStagingURL();
            this.casinoURL = casino.getCasinoURL();
            this.isCasinoEnabled = casino.isCasinoEnabled();
            this.operatorId = casino.getOperator().getOperatorId();
            this.currencies = casino.getCurrencies().stream().map( currency -> new CurrencyDTO( currency ) ).collect( Collectors.toList() );
            this.validGames = casino.getGames().stream().map(game -> game.getGameId(  )).collect( Collectors.toList());
            this.productContacts = casino.getProductContacts().stream().map(contact -> new ProductContactDTO( contact )).collect(Collectors.toList());
        }

    }


    public Casino convertCasino() {

        Casino casino = new Casino();
        casino.setName( this.name );
        casino.setCode( this.code );
        casino.setCasinoId( this.casinoId );
        casino.setSoftlock( this.softlock );
        casino.setReportEnabled( this.reportEnabled );
        casino.setRecoverSessionEnabled( this.recoverSessionEnabled );
        casino.setDesktopTemplate( this.desktopTemplate );
        casino.setMobileTemplate( this.mobileTemplate );
        // this.currencies.stream().map( currency -> new CurrencyDTO( currency )
        // ).collect( Collectors.toList() );
        return casino;
    }

}
