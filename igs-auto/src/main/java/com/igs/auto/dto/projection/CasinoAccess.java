package com.igs.auto.dto.projection;

public interface CasinoAccess {

	String ACCEPT_HEADER = "application/vnd.casino-access+json";

	String getApiStagingURL( );
	String getCasinoLogin( );
	String getCasinoPwd( );
	String getCasinoLastEnableDate();
	String getApiProdURL();
	String getCasinoURL();
	String getName();

}
