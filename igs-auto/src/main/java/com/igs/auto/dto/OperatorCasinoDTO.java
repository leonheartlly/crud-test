package com.igs.auto.dto;


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ObjectUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.igs.auto.domain.Casino;
import com.igs.auto.domain.Game;

import lombok.Data;


@Data
public class OperatorCasinoDTO implements Serializable {

    private Long id;

    private String operatorId;

    private String name;

    private String code;

    private String casinoId;

    private String softlock;

    private boolean reportEnabled;

    private boolean recoverSessionEnabled;

    private String desktopTemplate;

    private String mobileTemplate;

    private boolean isCasinoEnabled;

    private Timestamp casinoLastEnableDate;

    private List< CurrencyDTO > currencies = new ArrayList<>();

    @JsonIgnore
    private List< Game > validGames = new ArrayList<>();


    public OperatorCasinoDTO() {

    }


    public OperatorCasinoDTO( Casino casino ) {

        if ( ObjectUtils.isNotEmpty( casino ) ) {
            this.id = casino.getId();
            this.name = casino.getName();
            this.operatorId = casino.getOperator().getOperatorId();
            this.code = casino.getCode();
            this.casinoId = casino.getCasinoId();
            this.softlock = casino.getSoftlock();
            this.reportEnabled = casino.isReportEnabled();
            this.isCasinoEnabled = casino.isCasinoEnabled();
            this.recoverSessionEnabled = casino.isRecoverSessionEnabled();
            this.desktopTemplate = casino.getDesktopTemplate();
            this.mobileTemplate = casino.getMobileTemplate();
            this.currencies = casino.getCurrencies().stream().map( currency -> new CurrencyDTO( currency ) ).collect( Collectors.toList() );
        }

    }


    public Casino convertCasino() {

        Casino casino = new Casino();
        casino.setId( this.id );
        casino.setName( this.name );
        casino.setCode( this.code );
        casino.setCasinoId( this.casinoId );
        casino.setSoftlock( this.softlock );
        casino.setReportEnabled( this.reportEnabled );
        casino.setRecoverSessionEnabled( this.recoverSessionEnabled );
        casino.setDesktopTemplate( this.desktopTemplate );
        casino.setMobileTemplate( this.mobileTemplate );
        casino.setCasinoEnabled( this.isCasinoEnabled );
        casino.setCasinoLastEnableDate( this.casinoLastEnableDate );
        // this.currencies.stream().map( currency -> new CurrencyDTO( currency )
        // ).collect( Collectors.toList() );
        return casino;
    }

}
