package com.igs.auto.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.igs.auto.domain.Currency;
import com.igs.auto.domain.Denom;
import com.igs.auto.utils.IgsUtils;

import lombok.Data;


@Data
public class DenomDTO {

    @JsonIgnore
    private Long id;

    //TODO criar custom validator
    @NotEmpty( message = "Denom cannot be empty." )
    private double[] denom;

    private List< Currency > currencies = new ArrayList<>();


    public DenomDTO() {

    }


    public DenomDTO( Long id, double[] denom ) {

        this.id = id;
        this.denom = denom;
    }


    public DenomDTO( Denom denom ) {

        this.id = denom.getId();
        try {
            this.denom = IgsUtils.convertDenom( denom.getDenoms() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
        this.currencies = denom.getCurrencies();
    }
}
