package com.igs.auto.dto.projection;

public interface CasinoDetail {

	String ACCEPT_HEADER = "application/vnd.casino-detail+json";

	String getApiStagingURL();
	String getCasinoLogin();
	String getCasinoPwd();
}
