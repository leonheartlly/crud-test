package com.igs.auto.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.igs.auto.domain.Bet;
import com.igs.auto.utils.IgsUtils;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotEmpty;


@Data
public class BetDTO {

    @JsonIgnore
    @Nullable
    private Long id;

    @NotEmpty( message = "Bet nickname cannot be empty." )
    private String nickname;

    @NotEmpty( message = "Bets cannot be empty." )
    private int[] bets;


    public BetDTO() {

    }


    public BetDTO( Long id, int[] bets, String key ) {

        this.id = id;
        this.bets = bets;
        this.nickname = key;
    }


    // TODO criar exception especifica
    public BetDTO( Bet bet ) {

        this.id = bet.getId();
        try {
            this.bets = IgsUtils.convertBet( bet.getBets() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
        this.nickname = bet.getNickname();
    }

}
