package com.igs.auto.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.igs.auto.domain.MathConfig;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class MathConfigInsertDTO {

    @NotEmpty( message = "Operator id cannot be empty." )
    @Pattern( regexp = "[0-9]{1,15}", message = "Wrong operator id pattern." )
    private String operatorId;

    // TODO cassinoID é um contador, pode ser automatizado.
    @NotEmpty( message = "Casino id cannot be empty." )
    @Pattern( regexp = "[0-9]{1,4}", message = "Wrong casino id pattern." )
    private String casinoId;

    @NotEmpty( message = "Cabinet cannot be empty." )
    @Length(min = 3, max = 4, message = "Currency should have a least 3 and max 4 characters.")
    private String currency;

    @NotEmpty( message = "Cabinet cannot be empty." )
    @Pattern( regexp = "[0-9]{1,3}", message = "Wrong cabinet pattern." )
    private String cabinet;


    private Long betId;

    @Positive(message = "Denom id must be higher than 0.")
    private Long denom;

    @JsonIgnore
    private double extraball_rtp;

    @JsonIgnore
    private String mathIdentifier;


    public MathConfigInsertDTO() {

    }


    public MathConfigInsertDTO( MathConfig mathConfig ) {

        this.operatorId = mathConfig.getOperatorId();
        this.casinoId = mathConfig.getCasinoId();
        this.cabinet = mathConfig.getCabinet();
        this.currency = mathConfig.getCurrency().getCurrency();
        this.mathIdentifier = operatorId + "-" + casinoId + "-" + currency;

    }


    public String generateMathIdentifier() {

        return operatorId + "-" + casinoId + "-" + currency;
    }
}