package com.igs.auto.dto;


import com.igs.auto.domain.Currency;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;


@Data
public class CurrencyDTO {

    @NotEmpty( message = "Currency cannot be empty." )
    @Length( min = 3, max = 4, message = "Currency must have minimum size of 3 and maximum 4" )
    @Pattern( regexp = "[a-zA-Z]{3}", message = "Currency acronym pattern do not match." )
    private String currency;

    @NotEmpty( message = "Currency name cannot be empty." )
    @Length( min = 4, max = 30, message = "Currency must have minimum size of 4 and maximum 30." )
    private String curr_name;

    @NotEmpty( message = "Currency symbol cannot be empty." )
    @Length( min = 1, max = 5, message = "Currency must have minimum size of 1 and maximum 5." )
    private String symbol;


    public CurrencyDTO() {

    }


    public CurrencyDTO( Long id, String Currency ) {

        this.currency = Currency;
    }


    public CurrencyDTO( Currency currency ) {

        this.currency = currency.getCurrency();
        this.curr_name = currency.getCurr_name();
        this.symbol = currency.getSymbol();
    }


    @Override
    public String toString() {

        return currency;
    }
}
