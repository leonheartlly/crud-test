package com.igs.auto.dto;


import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.igs.auto.domain.Denom;
import com.igs.auto.utils.IgsUtils;

import lombok.Data;


@Data
public class DenomInsertDTO {

    @JsonIgnore
    private Long id;

    @NotEmpty( message = "Denom cannot be empty." )
    private double[] denom;

    @NotEmpty( message = "A valid currency is required." )
    private String currency;


    public DenomInsertDTO() {

    }


    public DenomInsertDTO( Long id, double[] denom ) {

        this.id = id;
        this.denom = denom;
    }


    public DenomInsertDTO( Denom denom ) {

        this.id = denom.getId();
        try {
            this.denom = IgsUtils.convertDenom( denom.getDenoms() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }
}
