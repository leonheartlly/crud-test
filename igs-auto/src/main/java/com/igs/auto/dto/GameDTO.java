package com.igs.auto.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.igs.auto.domain.Game;
import com.igs.auto.domain.enums.GameType;

import lombok.Data;


@Data
public class GameDTO {

    @JsonIgnore
    private Long id;

    @NotEmpty( message = "Game name cannot be empty." )
    @Length( min = 4, max = 35, message = "Game name must have minimum size of 4 and maximum 35." )
    private String game;

    @NotEmpty( message = "Game id cannot be empty." )
    @Pattern( regexp = "[0-9]{1,3}", message = "Wrong game id pattern." )
    private String gameId;

    private GameType gameType;

    @Positive( message = "Ortiznet model number cannot be this low." )
    private int ortizNetModelNumber;


    public GameDTO() {

    }


    public GameDTO( String game ) {

        this.game = game;
    }


    public GameDTO( Game game ) {

        this.id = game.getId();
        this.game = game.getGame();
        this.gameId = game.getGameId();
        this.gameType = game.getGameType();
        this.ortizNetModelNumber = game.getOrtizNetModelNumber();
    }

}
