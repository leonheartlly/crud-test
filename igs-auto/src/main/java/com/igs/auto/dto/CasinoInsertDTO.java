package com.igs.auto.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.igs.auto.domain.Casino;

import lombok.Data;


@Data
public class CasinoInsertDTO implements Serializable {

    @JsonIgnore
    private Long id;

    @NotEmpty( message = "Casino name cannot be empty." )
    @Length( min = 4, max = 35, message = "Casino must have minimum size of 4 and maximum 35" )
    private String name;

    @NotEmpty( message = "Casino code cannot be empty." )
    @Pattern( regexp = "[a-zA-Z]{2,3}", message = "Wrong code pattern." )
    private String code;

    @NotEmpty( message = "Casino id cannot be empty." )
    @Pattern( regexp = "[0-9]{1,3}", message = "Wrong casino id pattern." )
    private String casinoId;

    @NotEmpty( message = "Casino softlock cannot be empty." )
    @Pattern( regexp = "[0-9]{4,6}", message = "Wrong softlock pattern." )
    private String softlock;

    private boolean reportEnabled;

    private boolean recoverSessionEnabled;

    @NotEmpty( message = "Casino desktop template cannot be empty." )
    private String desktopTemplate;

    @NotEmpty( message = "Casino mobile template cannot be empty." )
    private String mobileTemplate;

    private boolean isCasinoEnabled;

    @JsonIgnore
    private Timestamp casinoLastEnableDate;

    @JsonIgnore
    private Timestamp casinoCreationDate;

    @Length( min = 0, max = 50, message = "Casino login must have minimum size of 0 and maximum 50." )
    private String casinoLogin;

    @Length( min = 0, max = 25, message = "Casino pwd must have minimum size of 0 and maximum 25." )
    private String casinoPwd;

    @Length( min = 0, max = 120, message = "Casino URL must have minimum size of 0 and maximum 120." )
    private String casinoURL;

    @Length( min = 0, max = 100, message = "Casino API staging URL must have minimum size of 0 and maximum 100." )
    private String apiStagingURL;

    @Length( min = 0, max = 100, message = "Casino API prod URL must have minimum size of 0 and maximum 100." )
    private String apiProdURL;

    @NotEmpty( message = "Casino currencies cannot be empty." )
    private List< String > currencies = new ArrayList<>();

    @NotEmpty( message = "Casino valid Games cannot be empty." )
    private List< String > validGames = new ArrayList<>();

    @NotEmpty( message = "Operator Id cannot be empty." )
    @Length( min = 0, max = 25, message = "Operator Id have minimum size of 0 and maximum 25." )
    private String operatorId;


    public CasinoInsertDTO() {

    }


    public CasinoInsertDTO( Casino casino ) {

        this.id = casino.getId();
        this.name = casino.getName();
        this.code = casino.getCode();
        this.casinoId = casino.getCasinoId();
        this.softlock = casino.getSoftlock();
        this.reportEnabled = casino.isReportEnabled();
        this.recoverSessionEnabled = casino.isRecoverSessionEnabled();
        this.desktopTemplate = casino.getDesktopTemplate();
        this.mobileTemplate = casino.getMobileTemplate();
        // this.currencies =
    }
}
