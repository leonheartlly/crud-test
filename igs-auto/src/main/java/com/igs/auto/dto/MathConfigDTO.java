package com.igs.auto.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.igs.auto.domain.Bet;
import com.igs.auto.domain.Currency;
import com.igs.auto.domain.Denom;
import com.igs.auto.domain.MathConfig;
import lombok.Data;


@Data
public class MathConfigDTO {

    @JsonIgnore
    private Long id;

    private String operatorId;

    private String casinoId;

    private Currency currency;

    private String cabinet;

    @JsonIgnore
    private int betMultiplier = 1;

    private Bet bet;

    private String betNickname;

    private Denom denom;

    @JsonIgnore
    private double extraball_rtp;

    @JsonIgnore
    private double contribution = -1;

    @JsonIgnore
    private String mathIdentifier;


    public MathConfigDTO( MathConfig mathConfig ) {

        this.operatorId = mathConfig.getOperatorId();
        this.casinoId = mathConfig.getCasinoId();
        this.cabinet = mathConfig.getCabinet();
        this.mathIdentifier = mathConfig.getMathIdentifier();
        this.bet = mathConfig.getBet();
        this.denom = mathConfig.getDenom();
        this.currency = mathConfig.getCurrency();
    }
}