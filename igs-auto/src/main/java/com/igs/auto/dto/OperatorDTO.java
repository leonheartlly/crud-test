package com.igs.auto.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.igs.auto.domain.Operator;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class OperatorDTO {

    @JsonIgnore
    private Long id;

    @NotEmpty( message = "Operator name cannot be empty." )
    @Length( min = 3, max = 35, message = "Currency must have minimum size of 3 and maximum 35." )
    private String name;

    @NotEmpty( message = "Operator id cannot be empty." )
    @Pattern( regexp = "[0-9]{1,15}", message = "Wrong operator id pattern." )
    private String operatorId;

    private boolean isTestingOperator;

    private boolean saveHistory;

    private boolean retryEnabled;

    private boolean instantDeposit;

    private boolean sendZeroDeposit;

    @NotEmpty( message = "GamePath cannot be empty." )
    private String gamePath;

    @NotEmpty( message = "Endpoint cannot be empty." )
    private String endpoint;


    // @JsonIgnore
    // private List< Casino > casinos = new ArrayList<>( );

    public OperatorDTO() {

    }


    public OperatorDTO( Operator operator ) {

        this.id = operator.getId();
        this.name = operator.getName();
        this.operatorId = operator.getOperatorId();
        this.isTestingOperator = operator.isTestingOperator();
        this.saveHistory = operator.isSaveHistory();
        this.retryEnabled = operator.isRetryEnabled();
        this.instantDeposit = operator.isInstantDeposit();
        this.sendZeroDeposit = operator.isSendZeroDeposit();
        this.gamePath = operator.getGamePath();
        this.endpoint = operator.getEndpoint();
    }
}
