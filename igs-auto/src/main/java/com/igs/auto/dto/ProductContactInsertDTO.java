package com.igs.auto.dto;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

import com.igs.auto.service.validator.ContactInsert;
import org.hibernate.validator.constraints.Length;

import com.igs.auto.domain.ProductContact;

import lombok.Data;
import org.springframework.lang.Nullable;


@Data
@ContactInsert
public class ProductContactInsertDTO {

    @NotEmpty( message = "Contact name cannot be empty." )
    @Length( min = 3, max = 50, message = "Contact name must have minimum size of 3 and maximum 50." )
    private String contactName;

    @NotEmpty( message = "Area cannot be empty." )
    @Length( min = 3, max = 25, message = "Area must have minimum size of 3 and maximum 25." )
    private String area;

    @Email( message = "Email must be a valid email." )
    @NotEmpty( message = "Email cannot be empty." )
    private String email;

    @NotEmpty( message = "Skype group cannot be empty." )
    @Length( min = 3, max = 50, message = "Skype Group must have minimum size of 3 and maximum 50." )
    private String skypeGroup;

    @Nullable
    private long casinoid;

    @Nullable
    private String operatorId;


    public ProductContactInsertDTO() {

    }


    public ProductContactInsertDTO( ProductContact productContact ) {

        this.contactName = productContact.getContactName();
        this.area = productContact.getArea();
        this.email = productContact.getEmail();
        this.skypeGroup = productContact.getSkypeGroup();

    }

}
