package com.igs.auto;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class IgsAutomatorApplication {

    public static void main( String[] args ) {

        SpringApplication.run( IgsAutomatorApplication.class, args );
    }

}
